import random

from words import wordlist

def create_pseudonym(lang, capitalize = False):
    words = get_4_random_words(lang)
    pseudonym = ""
    for word in words:
        if capitalize:
            word = word.capitalize()
        else:
            word = word.lower()
        pseudonym += word
    return pseudonym

def get_4_random_words(lang):
    words = []
    word1 = random.choice(wordlist[lang])
    words.append(word1)
    word2 = get_another_word(words, lang)
    words.append(word2)
    word3 = get_another_word(words, lang)
    words.append(word3)
    word4 = get_another_word(words, lang)
    words.append(word4)
    return words

def get_another_word(list, lang):
    word = random.choice(wordlist[lang])
    while(word in list):
        word = random.choice(wordlist[lang])
    return word

def get_readme(host):

    readme = """
             <h1> Pseudonym Provider </h1>

             <h2> Start </h2>
             docker-compose -f docker-compose.complete.yml up --build<br>

             <h2> Usage </h2>

             You can get new pseudonyms when sending a get request to `{host}pseudonym/`.<br>
             This will give you a json response like the following:<br>
             <div class="code">
             {{"pseudonym":"FallInformationFrauWoche"}}
             </div>
             The pseudonym is generated in the default language German. If you want to have another language you have to add the<br>
             language code as further parameter like `{host}pseudonym/en`<br>

             At the moment the following requests are possible:<br>
             <div class="code">
             {host}pseudonym/<br>
             {host}pseudonym/de<br>
             {host}pseudonym/en<br>
             {host}pseudonym/it<br>
             </div>

             <h3> Capitalized words </h3>
             The language determines if the four nouns within the pseudonym are capitalized or not. In German<br>
             every noun is written with a capital letter. So the pseudonyms look like "FallInformationFrauWoche".<br>
             In English the nouns are not written with a capital letter so the pseudonyms look like "mindwebnamestory".<br>
             If you want to have capitalized words you need to add a query parameter like<br>
             <div class="code">
             {host}pseudonym/en?capitalize=true<br>
             </div>
             Then the pseudonym would be "MindWebNameStory". It is also possible to not have the German words capitalized with:<br>
             <div class="code">
             {host}pseudonym/de?capitalize=false<br>
             </div>

             **Attention:** Internal the pseudonyms are the same no matter if they have uppercase or lowercase letters. (mindwebnamestory == MindWebNameStory)<br>

             <h2> Development </h2>

             <h3> add language </h3>
             To add an language create a csv file in the words folder. It must be of the form:<br>

             <div class="code">
             ;word list generated with Sketch Engine https://www.sketchengine.co.uk/word-lists/;;<br>
             ;language:;English;<br>
             ;corpus:;English Web 2013;<br>
             ;noun;f<br>
             1;time;<br>
             2;year;<br>
             3;people;<br>
             </div>

             So in line 5 column 2 begin the words.<br>
             In the file `settings.py` add the language and the just added file to the language dict<br>
             <div class="code">
             LANGUAGES: dict = {{<br>
                     'en': 'words/english.csv',<br>
                     ...<br>
             }}<br>
             </div>
             In `run.py` add the route as follows:<br>
             <div class="code">
             @app.get("/pseudonym/en")<br>
             def get_pseudonym_en():<br>
                 return generate_pseudonym('url', 'en')<br>
             </div>
    """.format(host=host)
    return readme
