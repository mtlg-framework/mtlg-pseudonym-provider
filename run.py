import uvicorn
import mongoengine
import re
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware
from pseudonym import create_pseudonym, get_readme
from settings import config
from models import Pseudonym
from starlette.requests import Request

app = FastAPI(title="MTLG Pseudonymprovider", root_path="/pseudo")
mongoengine.connect("pseudonymprovider", host=config.MONGODB_CONNECTION)
app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

@app.get("/")
async def root(request: Request):
    html_content = """
                       <html>
                           <head>
                               <title>Pseudonym Provider</title>
                           </head>
                           <body>
                           <span>
                               {body}
                           </span>
                           </body>
                           <style>
                           .code {{
                                margin: 10px;
                                border: solid 1px black;
                                width: 40rem;
                                padding: 10px;
                           }}
                           </style>
                       </html>
                       """.format(body=get_readme(request.url))
    return HTMLResponse(content=html_content, status_code=200)

@app.get("/pseudonym")
def get_pseudonym(capitalize: bool = True, special: bool = False):
    return generate_pseudonym('url', config.default_language, capitalize, special)

@app.get("/pseudonym/en")
def get_pseudonym_en(capitalize: bool = False, special: bool = False):
    return generate_pseudonym('url', 'en', capitalize, special)

@app.get("/pseudonym/de")
def get_pseudonym_de(capitalize: bool = True, special: bool = False):
    return generate_pseudonym('url', 'de', capitalize, special)

@app.get("/pseudonym/it")
def get_pseudonym_it(capitalize: bool = False, special: bool = False):
    return generate_pseudonym('url', 'it', capitalize, special)

def check_special_char(special_allowed, pseudonym):
    return (special_allowed or re.match("^[A-Za-z]+$",pseudonym))

def generate_pseudonym(req,lang, capitalize = False, special_allowed = False):
    # generate string
    pseudonym = create_pseudonym(lang, capitalize)
    duplicate = Pseudonym.objects(pseudonym__iexact=pseudonym)

    valid_chars = check_special_char(special_allowed, pseudonym)

    i = 100
    while((len(duplicate)>0 or not valid_chars) and i > 0):
        pseudonym = create_pseudonym(lang, capitalize)
        duplicate = Pseudonym.objects(pseudonym__iexact=pseudonym)
        valid_chars = check_special_char(special_allowed, pseudonym)

        i -= 1
    if(i == 0):
        return {"error": "No unique pseudonym found"}

    #create object
    pseudonymObj = Pseudonym(requester=req, language=lang, pseudonym=pseudonym)
    pseudonymObj.save()
    return {"pseudonym": pseudonymObj.pseudonym}

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8001, reload=True)
