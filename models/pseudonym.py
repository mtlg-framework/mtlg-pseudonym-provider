import datetime
from mongoengine import Document, DateTimeField, StringField

class Pseudonym(Document):
    created_at = DateTimeField(default=datetime.datetime.utcnow)
    pseudonym = StringField()
    requester = StringField()
    language = StringField()
