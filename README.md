# Pseudonym Provider

## Start
docker-compose -f docker-compose.complete.yml up --build

## Usage

You can get new pseudonyms when sending a get request to `http://localhost:8001/pseudonym/`.
This will give you a json response like the following: 
```
{"pseudonym":"FallInformationFrauWoche"}
```
The pseudonym is generated in the default language German. If you want to have another language you have to add the 
language code as further parameter like `http://localhost:8001/pseudonym/en`

At the moment the following requests are possible:
```
http://localhost:8001/pseudonym/
http://localhost:8001/pseudonym/de
http://localhost:8001/pseudonym/en
http://localhost:8001/pseudonym/it
```

### Capitalized words
The language determines if the four nouns within the pseudonym are capitalized or not. In German
every noun is written with a capital letter. So the pseudonyms look like "FallInformationFrauWoche".
In English the nouns are not written with a capital letter so the pseudonyms look like "mindwebnamestory".
If you want to have capitalized words you need to add a query parameter like
```
http://localhost:8001/pseudonym/en?capitalize=true 
```
Then the pseudonym would be "MindWebNameStory". It is also possible to not have the German words capitalized with:
```
http://localhost:8001/pseudonym/de?capitalize=false 
``` 

**Attention:** Internal the pseudonyms are the same no matter if they have uppercase or lowercase letters. (mindwebnamestory == MindWebNameStory)

### Special Characters
As there are languages like german or italian with special characters (e.g. ä,á,...), we also provide the functionality to generate pseudonyms with or without such characters to avoid problems when working with certain standards (like ASCII).

If you explicitly want special characters you need to add the following query parameter 
```
http://localhost:8001/pseudonym/de?special=true 
```

If you don't want special character you can add the following query paramter (or leave it as this is the default value)
```
http://localhost:8001/pseudonym/de?special=false
```

**Note:** If you want to combine multiple query parameters you can do this in the following way 
```
http://localhost:8001/pseudonym/de?capitalize=true&special=true
```

## Development

### add language
To add an language create a csv file in the words folder. It must be of the form:

```
;word list generated with Sketch Engine https://www.sketchengine.co.uk/word-lists/;;
;language:;English;
;corpus:;English Web 2013;
;noun;f
1;time;
2;year;
3;people;
```

So in line 5 column 2 begin the words. 
In the file `settings.py` add the language and the just added file to the language dict
```
LANGUAGES: dict = {
        'en': 'words/english.csv',
        ...
}
```
In `run.py` add the route as follows:
```
@app.get("/pseudonym/en")
def get_pseudonym_en():
    return generate_pseudonym('url', 'en')
```
