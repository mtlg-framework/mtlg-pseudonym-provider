import csv
from settings import config

wordlist = {}


for lang in config.LANGUAGES:
    file = config.LANGUAGES[lang]
    wordlist[lang] = []
    with open(file, newline='', encoding='utf-8') as f:
        reader = csv.reader(f, delimiter=";")
        data = list(reader)

    data = data[4:]


    for line in data:
        if(len(line) > 1):
            wordlist[lang].append(line[1])
        else:
            wordlist[lang].append(line)
