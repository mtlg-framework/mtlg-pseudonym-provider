class MTLG_Config():
    MONGODB_CONNECTION: str = "mongodb://mongodb/pseudonymprovider"
    LANGUAGES: dict = {
        'en': 'words/english.csv',
        'de': 'words/german.csv',
        'it': 'words/italian.csv'
    }
    default_language: str = 'de'

config = MTLG_Config()
